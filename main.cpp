#include <stdio.h>

#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>
#include <opencv2//imgproc//imgproc.hpp>

typedef int (*altaOrdemFunc)(int);

#define CALCULAINDICE( widthStep , nChannels , linha , coluna )  ((linha)*(widthStep))+((coluna)*(nChannels))

#define P(x) p[x-1]

#define CALC_P1 P(1) = CALCULAINDICE(image->widthStep, image->nChannels, linha, coluna);
#define CALC_P2 P(2) = CALCULAINDICE(image->widthStep, image->nChannels, (linha-1), coluna);
#define CALC_P3 P(3) = CALCULAINDICE(image->widthStep, image->nChannels, (linha-1), (coluna+1));
#define CALC_P4 P(4) = CALCULAINDICE(image->widthStep, image->nChannels, linha, (coluna+1));
#define CALC_P5 P(5) = CALCULAINDICE(image->widthStep, image->nChannels,(linha+1), (coluna+1));
#define CALC_P6 P(6) = CALCULAINDICE(image->widthStep, image->nChannels,(linha+1), coluna);
#define CALC_P7 P(7) = CALCULAINDICE(image->widthStep, image->nChannels,(linha+1), (coluna-1));
#define CALC_P8 P(8) = CALCULAINDICE(image->widthStep, image->nChannels, linha, (coluna-1));
#define CALC_P9 P(9) = CALCULAINDICE(image->widthStep, image->nChannels, (linha-1),(coluna-1));
#define CALC_PS CALC_P1; CALC_P2; CALC_P3; CALC_P4; CALC_P5; CALC_P6; CALC_P7; CALC_P8; CALC_P9;

int soma(int valor){
	
	return valor + 1;
}

int sub(int valor){
	
	return valor - 1;
}

int processador(altaOrdemFunc funcao, int valor){
	
	printf("%d", funcao(valor));
	return funcao(valor);
}

int somaMagnitude(IplImage* image, uchar* imageBytes, int imagePos){

	//int p[9],linha, coluna;

	float kernelx[3][3] = {{-1, 0, 1}, 
						   {-2, 0, 2}, 
						   {-1, 0, 1}};

	float kernely[3][3] = {{-1, -2, -1}, 
						   {0,  0,  0}, 
						   {1,  2,  1}};

	int x = imagePos % image->width;
	int y = imagePos / image->width;

	double magX = 0.0;
	double magY = 0.0;

	for(int a = 0; a < 3; a++){
		for(int b = 0; b < 3; b++)		{            
			int xn = x + a - 1;
			int yn = y + b - 1;
					
			//CALC_PS;

			int index = xn + yn * image->width;

			magX += imageBytes[index] * kernelx[a][b];
			magY += imageBytes[index] * kernely[a][b];
				
		}
	}

	int result = sqrt(magX*magX + magY*magY);

	if ((result < 200))
		if(result > 150)
			result = 255;
		else
			result = result/2;
	

	return result;//sqrt(magX*magX + magY*magY);
}

int main(int argc, char ** argv){
	IplImage *imageRaw, *imageResult;
	unsigned char* image, *result;
	imageRaw = cvLoadImage("img.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	image = (unsigned char*)imageRaw->imageData;	

	cvShowImage("Original", imageRaw);

	int tamanho = imageRaw->width*imageRaw->height;

	printf("nSize: %d - imageSize: %d\n", imageRaw->nSize, sizeof(unsigned char)*imageRaw->imageSize);

	result = (unsigned char*) malloc(sizeof(unsigned char)*imageRaw->imageSize);

	for(int z = imageRaw->width; z < tamanho-imageRaw->width; z++){

		result[z] = somaMagnitude(imageRaw, image, z);
	}

	imageResult = cvCreateImageHeader(cvSize(imageRaw->width,imageRaw->height), IPL_DEPTH_8U, 1);
	cvSetData(imageResult,result,imageRaw->widthStep);

	cvShowImage("Original", imageRaw);
	cvShowImage("Sobel", imageResult);
		
	cvWaitKey(100000);

	return processador(soma, 0);
}